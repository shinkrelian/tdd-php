<?php

require_once '../src/Observer.php';
require_once '../src/SomeClass.php'; 

class SubjectTest extends PHPUnit_Framework_TestCase
{
    //Verificar que se llama con algun valor especifico
    public function testObserversAreUpdated()
    {

	// Crear el mock
	// solo mockear el metodo update
	$observer = $this->getMockBuilder('Observer')
                 ->setMethods(array('update'))
                 ->getMock();

	// Setar la expectativa del metodo
	// que sea llamado solo una vez y con la cadena 'something'
	// como parametro
	$observer->expects($this->once())
        	->method('update')
         	->with($this->equalTo('something'));

	// Crear el objeto Subjeto  y atachar el observador 
	// mockeado
	$subject = new Subject('My subject');
	$subject->attach($observer);

	// Llamar al metodo doSomething del sujeto
	// que deberia llamar al mentodo 
	// update() del observador con la cadena 'something'.
	$subject->doSomething();

        $this->assertFalse(TRUE);
    }
 
    //Verificar que un metodo es llamado con argumentos que 
    //cumplan ciertas condiciones	
    public function testErrorReported()
    {
        // Crear un mock del observer mockeando el metdodo
        // reortError() 
        $observer = $this->getMockBuilder('Observer')
                         ->setMethods(array('reportError'))
                         ->getMock();

 	//Setear al expectativa
	$observer->expects($this->once())
	 	->method('reportError')
        	 ->with(
               		$this->greaterThan(0),
               		$this->stringContains('Something'),
               		$this->anything()
           	);

	$subject = new Subject('My subject');
	$subject->attach($observer);


	$subject->doSomethingBad();
    }

    //Verificar que un metodo es llamado 2 veces
    //con valores epecificos
    public function testFunctionCalledTwoTimesWithSpecificArguments()
    {
        $mock = $this->getMockBuilder('SomeClass')
                     ->setMethods(array('doSomething'))
                     ->getMock();

        $mock->expects($this->exactly(2))
             ->method('doSomething')
             ->withConsecutive(
                 array($this->equalTo('foo'), $this->greaterThan(0)),
                 array($this->equalTo('bar'), $this->greaterThan(0))
	);

        $mock->doSomething('foo', 21);
        $mock->doSomething('bar', 48);
    }

    
    //Verifica unica llamada y con isntacia especifac 
    //como parametro
    public function testIdenticalObjectPassed()
    {
	$expectedObject = new SomeClass();

	$mock = $this->getMockBuilder('SomeClass')
             ->setMethods(array('doSomething'))
             ->getMock();

	$mock->expects($this->once())
     		->method('doSomething')
     		->with($this->identicalTo($expectedObject));

	$mock->doSomething($expectedObject);
    }
}
?>
